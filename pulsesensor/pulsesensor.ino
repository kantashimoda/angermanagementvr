#define USE_ARDUINO_INTERRUPTS true
#include <PulseSensorPlayground.h>

const int PulseWire = 0; // パルスセンサーのピン番号    
const int LED13 = 13;  // LEDのピン番号       
int Threshold = 550;   // 取得する信号の閾値     
                               
PulseSensorPlayground pulseSensor; 


void setup() {   
 // シリアル通信のbps設定
  Serial.begin(115200);          

  pulseSensor.analogInput(PulseWire);   
  pulseSensor.blinkOnPulse(LED13);  // LEDが心拍数に合わせて自動で点滅するようにする  
  pulseSensor.setThreshold(Threshold);   

  pulseSensor.begin();
}



void loop() {
 
  int myBPM = pulseSensor.getBeatsPerMinute();    //心拍数を取得
 
  if (pulseSensor.sawStartOfBeat()) {  //心拍数を取得できているなら  
   Serial.println(myBPM); //データを送信する        
  }
  delay(20);  // 20ミリ秒待機する     
}
