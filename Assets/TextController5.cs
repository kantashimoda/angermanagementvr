﻿
 using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextController5 : MonoBehaviour
{
    public string[] scenarios;
    string source = "ストップ。ちょっと落ち着こう。リラックス、リラックス。, きみは誰？, あなたの怒りのコントロールのお手伝いをしに来たの。,";
    public string[] splitted;
    char[] separator = new char[] { ',' };


    [SerializeField] Text uiText;

    [SerializeField]
    [Range(0.001f, 10f)]//ここの左の～fを変えると一文字の表示にかかる時間が変わる
    float intervalForCharacterDisplay = 0.05f;

    private string currentText = string.Empty;
    private float timeUntilDisplay = 0;
    private float timeElapsed = 1;
    private int currentLine = 0;
    private int lastUpdateCharacter = -1;
    private int returnCount = 0;

    float TimeCount = 5;

    // 文字の表示が完了しているかどうか
    public bool IsCompleteDisplayText
    {
        get { return Time.time > timeElapsed + timeUntilDisplay; }
    }

    void Start()
    {

        SetNextLine();
    }

    void Update()
    {

        if (returnCount >= 3)
        {
            SceneManager.LoadScene("Virtual_space4");

        }
        TimeCount -= Time.deltaTime;


        // 文字の表示が完了してるならクリック時に次の行を表示する
        if (IsCompleteDisplayText)
        {
            if (currentLine < scenarios.Length && TimeCount <= 0)
            {

                if (returnCount <= 2)//returnCount <= ? ?の中に入れる数にセリフの数＋　２　
                {
                    SetNextLine();
                    TimeCount = 10;

                    returnCount++;
                }
                else if (returnCount <= 3)
                {
                    SetNextLine();
                    TimeCount = 3;

                    returnCount += 2;
                }



            }
        }



        int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
        if (displayCharacterCount != lastUpdateCharacter)
        {
            uiText.text = currentText.Substring(0, displayCharacterCount);
            lastUpdateCharacter = displayCharacterCount;
        }
    }


    void SetNextLine()
    {
        scenarios = source.Split(separator);
        currentText = scenarios[currentLine];
        timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
        timeElapsed = Time.time;
        currentLine++;
        lastUpdateCharacter = -1;
    }
}