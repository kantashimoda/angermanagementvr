﻿
// TextController1とほぼ同じです

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextController4 : MonoBehaviour
{
    public string[] scenarios;
    string source = "あれ？元にもどったぞ？怒りも収まってきたみたいだ。。。";
    public string[] splitted;
    char[] separator = new char[] { ',' };


    [SerializeField] Text uiText;

    [SerializeField]
    [Range(0.001f, 10f)]
    float intervalForCharacterDisplay = 0.05f;

    private string currentText = string.Empty;
    private float timeUntilDisplay = 0;
    private float timeElapsed = 1;
    private int currentLine = 0;
    private int lastUpdateCharacter = -1;
    private int SerifCount = 0;

    float TimeCount = 5;

   
    public bool IsCompleteDisplayText
    {
        get { return Time.time > timeElapsed + timeUntilDisplay; }
    }

    void Start()
    {

        SetNextLine();
    }

    void Update()
    {

        if (SerifCount >= 10)
        {
            SceneManager.LoadScene("Virtual_space5");

        }
        TimeCount -= Time.deltaTime;

        
        if (IsCompleteDisplayText)
        {
            if (currentLine < scenarios.Length && TimeCount <= 0)
            {

                if (SerifCount <= 8)　
                {
                    SetNextLine();
                    TimeCount = 5;

                    SerifCount++;
                }
                else if (SerifCount <= 9)
                {
                    SetNextLine();
                    TimeCount = 3;

                    SerifCount += 2;
                }



            }
        }



        int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
        if (displayCharacterCount != lastUpdateCharacter)
        {
            uiText.text = currentText.Substring(0, displayCharacterCount);
            lastUpdateCharacter = displayCharacterCount;
        }
    }


    void SetNextLine()
    {
        scenarios = source.Split(separator);
        currentText = scenarios[currentLine];
        timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
        timeElapsed = Time.time;
        currentLine++;
        lastUpdateCharacter = -1;
    }
}