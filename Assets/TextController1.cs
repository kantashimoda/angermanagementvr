﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextController1 : MonoBehaviour
{
    public string[] scenarios; // シナリオを格納する                         1                                                                          2                                             3                                               4                                                      5                                              6                                    7        8                                                       9                                                                 10                                                   11                                                12                13                         14                                                   15                                 16                                                                                  17                                                                                   18                                                       19 
    string source = "あなたが手に付けているセンサーは心拍数を測定しています。,あなたが怒りを感じていたり、ストレスがたまっていたりすると心拍数が上がり、,落ち着いてリラックスすると心拍数が下がります。,心拍数の変化を感じながら、ＶＲを見てください。,あなた今日もまた先生の授業を聞いてなかったでしょ！！！,そんなことだからテストで良い点が取れないのよ！,なんなんですかそのふてくされた顔は！,        ,あれ？ここはどこだろう？どうしちゃったんだろう......  ,よいしょっと!私はアンガーマネジメントの妖精、ユニティちゃんだよ！  ,なーんかイライラしてるね。リラックス、リラックス。  ,まずは深呼吸をしよう。吸って吸って吸って（３秒）,（１秒息を止める）,はいてはいてはいて（３秒）, 肩を思いっきりあげよう。上げてあげてあげて（５秒） ,肩の力を抜こう。はあー（１０秒） ,怒りが落ち着いてきたみたいだね。またイライラしたときはいつでも呼んでね。まったねー！, ,あれ？元にもどったぞ？怒りも収まってきたみたいだ。。。 ";
        
    public string[] splitted;
    char[] separator = new char[] { ',' };


    [SerializeField] Text uiText;

    [SerializeField]
    [Range(0.001f, 10f)]
    float intervalForCharacterDisplay = 0.05f;// 1文字の表示にかかる時間

    private string currentText = string.Empty;   // 現在の文字列
    private float timeUntilDisplay = 0;          // 表示にかかる時間
    private float timeElapsed = 3;               // 文字列の表示を開始した時間
    private int currentLine = 0;                 // 現在の行番号
    private int lastUpdateCharacter = -1;        // 表示中の文字数
    private int SerifCount = 0;                 //いくつめのセリフか

    float TimeCount = 5;                         //次のセリフを表示するまでの時間

    // 文字の表示が完了しているかどうか
    public bool IsCompleteDisplayText
    {
        get { return Time.time > timeElapsed + timeUntilDisplay; }
    }
    void Start()

    {

        SetNextLine();
    }

    void Update()
    {

        TimeCount -= Time.deltaTime;


        
        if (IsCompleteDisplayText)
        {
            if (currentLine < scenarios.Length && TimeCount <= 0)//行数が達し時間が来るとセリフを表示
            {
                if(SerifCount <= 1 || SerifCount > 2 && SerifCount <= 7 || SerifCount > 8 && SerifCount <= 17)
                {
                    TimeCount = 4;
                }
                if(SerifCount == 2)
                {
                    TimeCount = 7;
                }
                if(SerifCount == 8)
                {
                    TimeCount = 3;
                }
               
                
                SetNextLine();
                SerifCount++;




            }
        }


        // クリックから経過した時間が想定表示時間の何%か確認し、表示文字数を出す
        int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);

        // 表示文字数が前回の表示文字数と異なるならテキストを更新する
        if (displayCharacterCount != lastUpdateCharacter)
        {
            uiText.text = currentText.Substring(0, displayCharacterCount);
            lastUpdateCharacter = displayCharacterCount;
        }
    }
    // テキストを更新する

    void SetNextLine()
    {
        scenarios = source.Split(separator);
        currentText = scenarios[currentLine];

        // 想定表示時間と現在の時刻をキャッシュ
        timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
        timeElapsed = Time.time;
        currentLine++;
        lastUpdateCharacter = -1;
    }
}