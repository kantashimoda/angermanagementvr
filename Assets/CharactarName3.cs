﻿
//CharactarName2とほぼ同じです。

using UnityEngine;
using System.Collections;
using UnityEngine.UI;	// uGUIの機能を使うお約束

public class CharactarName3 : MonoBehaviour
{

    public string[] scenarios; // シナリオを格納する
    public Text uiText; // uiTextへの参照を保つ

    int currentLine = 0; // 現在の行番号

    string source = "  ,妖精,妖精,妖精";
    public string[] splitted;
    char[] separator = new char[] { ',' };
    private int SerifCount = 0;

    float TimeCount = 5;

    void Start()
    {
        TextUpdate();
    }

    void Update()
    {
        TimeCount -= Time.deltaTime;

        // 現在の行番号がラストまで行ってない状態でクリックすると、テキストを更新する
        if (currentLine < scenarios.Length && TimeCount <= 0)
        {
            if (SerifCount == 1 || SerifCount == 2)//returnCount <= ? ?の中に入れる数にセリフの数＋　２　
            {
                TextUpdate();
                TimeCount = 10;
            }
            SerifCount++;
        }
    }

    // テキストを更新する
    void TextUpdate()
    {
        scenarios = source.Split(separator);
        // 現在の行のテキストをuiTextに流し込み、現在の行番号を一つ追加する
        uiText.text = scenarios[currentLine];
        currentLine++;
    }
}