﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;	

public class CharactarName2 : MonoBehaviour
{

    public string[] scenarios; // シナリオを格納する
    public Text uiText; // uiTextへの参照を保つ

    int currentLine = 0; // 現在の行番号

    string source = "先生,自分,先生";
    public string[] splitted;
    char[] separator = new char[] { ',' };
    private int SerifCount = 0;　//いくつめのセリフか
      
    float TimeCount = 5;          //次のセリフを表示するまでの時間

    void Start()
    {
        TextUpdate();
    }

    void Update()
    {
        TimeCount -= Time.deltaTime;

        
        if (currentLine < scenarios.Length && TimeCount <= 0)
        {
            if (SerifCount == 1 || SerifCount == 2)
            {
                TextUpdate();
                TimeCount = 10;
            }
            SerifCount++;
        }
    }
    // テキストを更新する
    void TextUpdate()
    {
        scenarios = source.Split(separator);
        uiText.text = scenarios[currentLine];
        currentLine++;
    }
}