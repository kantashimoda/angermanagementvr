﻿
// TextController1とほぼ同じです

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextController3 : MonoBehaviour
{
    public string[] scenarios;//                                          1                                                                    2                                                    3                                                 4                  5                          6                                                  7                                 8                               9　　　　　　　　　　　　　　　　　　　　　　　　　　10
    string source = "あれ？ここはどこだろう？どうしちゃったんだろう......  ,よいしょっと!私はアンガーマネジメントの妖精、ユニティちゃんだよ！  ,なーんかイライラしてるね。リラックス、リラックス。  ,まずは深呼吸をしよう。吸って吸って吸って（３秒）,（１秒息を止める）,はいてはいてはいて（３秒）, 肩を思いっきりあげよう。上げてあげてあげて（５秒） ,肩の力を抜こう。はあー（１０秒） ,怒りが落ち着いてきたみたいだね。またイライラしたときはいつでも呼んでね。まったねー！, ";
    public string[] splitted;
    char[] separator = new char[] { ',' };


    [SerializeField] Text uiText;

    [SerializeField]
    [Range(0.001f, 10f)]//ここの左の～fを変えると一文字の表示にかかる時間が変わる
    float intervalForCharacterDisplay = 0.05f;

    private string currentText = string.Empty;
    private float timeUntilDisplay = 0;
    private float timeElapsed = 1;
    private int currentLine = 0;
    private int lastUpdateCharacter = -1;
    private int SerifCount = 0;

    float TimeCount = 5;


    // 文字の表示が完了しているかどうか
    public bool IsCompleteDisplayText
    {
        get { return Time.time > timeElapsed + timeUntilDisplay; }
    }

    void Start()
    {

        SetNextLine();
    }

    void Update()
    {

        if (SerifCount >= 9)
        {
            SceneManager.LoadScene("Virtual_space5");

        }
        TimeCount -= Time.deltaTime;


        // 文字の表示が完了してるならクリック時に次の行を表示する
        if (IsCompleteDisplayText)
        {
            if (currentLine < scenarios.Length && TimeCount <= 0)
            {
                SetNextLine();
                if (SerifCount <= 1)//returnCount <= ? ?の中に入れる数にセリフの数＋　２　
                {

                    
                    TimeCount = 10;

                   
                }
                else if (SerifCount <= 3)
                {
                    
                    TimeCount = 3;

                    
                }
                else if(SerifCount <= 4)
                {
                    TimeCount = 1;
                }
                else if(SerifCount <= 7)
                {
                    TimeCount = 3;
                }
                else if(SerifCount <= 8)
                {
                    TimeCount = 10;
                }

                SerifCount++;



            }
        }



        int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
        if (displayCharacterCount != lastUpdateCharacter)
        {
            uiText.text = currentText.Substring(0, displayCharacterCount);
            lastUpdateCharacter = displayCharacterCount;
            //Debug.Log(uiText.text);
        }
    }


    void SetNextLine()
    {
        scenarios = source.Split(separator);
        currentText = scenarios[currentLine];
        timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
        timeElapsed = Time.time;
        currentLine++;
        lastUpdateCharacter = -1;
    }
}