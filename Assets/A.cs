﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class A : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("Sample");
        StartCoroutine("Sample2");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator Sample()
    {
        Debug.Log("1");
        yield return new WaitForSeconds(1.0f);
        Debug.Log("2");
        yield return new WaitForSeconds(2.0f);
        Debug.Log("3");
        yield return new WaitForSeconds(3.0f);
    }
    private IEnumerator Sample2()
    {
        Debug.Log("4");
        yield return new WaitForSeconds(1.0f);
        Debug.Log("5");
        yield return new WaitForSeconds(2.0f);
        Debug.Log("6");
        yield return new WaitForSeconds(3.0f);
    }
    //２つが同時に行われる　例：Sample1ではセリフの進行、Sample2ではアニメーションを実行
}
