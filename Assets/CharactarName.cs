﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;	// uGUIの機能を使うお約束

public class CharactarName : MonoBehaviour
{

    public string[] scenarios; // シナリオを格納する
    public Text uiText; // uiTextへの参照を保つ

    int currentLine = 0; // 現在の行番号

    string source = " , ,先生, ,自分";
    public string[] splitted;
    char[] separator = new char[] { ',' };
    private int returnCount = 0;
  
    float TimeCount = 5;

    void Start()
    {
        TextUpdate();
    }

    void Update()
    {
        TimeCount -= Time.deltaTime;

        // 現在の行番号がラストまで行ってない状態でクリックすると、テキストを更新する
        if (currentLine < scenarios.Length && TimeCount <= 0)
        {
            if (returnCount == 0)
            {
                TimeCount = 17;
            }
            else if (returnCount == 1) {
                TimeCount = 15;
            }
            else if (returnCount == 2)
            {
                TimeCount = 1;
            }
            else if (returnCount == 4)
            {
                TimeCount = 23;
            }
            TextUpdate();
            returnCount++;
        }
    }

    // テキストを更新する
    void TextUpdate()
    {
        scenarios = source.Split(separator);
        // 現在の行のテキストをuiTextに流し込み、現在の行番号を一つ追加する
        uiText.text = scenarios[currentLine];
        currentLine++;
    }
}