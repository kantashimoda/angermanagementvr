﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Practice : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // コルーチンを実行  
        StartCoroutine("Sample");
    }

    // Update is called once per frame
    void Update()
    {



    }
    // コルーチン  
    private IEnumerator Sample()
    {

        // コルーチンの処理
        yield return new WaitForSeconds(20.0f);//説明画面
        for (int i = 0; i < 5; i++)
        {
            this.gameObject.transform.Translate(0, 0, 1f);
        }
        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i <= 54; i++)
        {
            this.gameObject.transform.Translate(0, 0, 1f);
        }//教室に移動
        yield return new WaitForSeconds(15.0f);
        for (int i = 0; i <= 54; i++)
        {
            this.gameObject.transform.Translate(0, 0, -1f);
        }
        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i <= 114; i++)
        {
            this.gameObject.transform.Translate(0, 0, 1f);
        }//森林に移動
        yield return new WaitForSeconds(45.0f);
        for (int i = 0; i <= 114; i++)
        {
            this.gameObject.transform.Translate(0, 0, -1f);
        }
        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i <= 54; i++)
        {
            this.gameObject.transform.Translate(0, 0, 1f);
        }

    }
}